+#git

**git**Git is the way we treat code and without git, there's no software creation.
*Git helps you to easily keep track of any revision you and your team create throughout your software development.


**repository** A repository is a list of files and directories that you use to track using git (code files). 
*It's the big box into which you and your team used to put your code.

- git init - _create a new loca repository_.
- git clone  : _clone the repository on local system_.
- git add . : _adds changes in the working directory to the staging area_.
- git commit : _saves the files from staging area into the local Git repository_.
- git push origin master : _saves the files in local repo onto the remote repo_.
- git pull - _fetch and merge changes on the remote server to your working directory_.

**Git Workflow**

clone your repo
>git cone repository-name

create a new branch

> git checkout master

> git checkout -b your-branch-name

modify files in your working tree.

Just those changes you want to be part of your next engagement are selectively staged.

>git add

the commit you made,which brings the files to your Local Git Repository as they are in the staging area and stores that are permanently snapshot.

>git commit -sv 

You push the files as they are in the Local Git Repository and store them permanently in your Remote Git Repository.

>git push origin branch-name



